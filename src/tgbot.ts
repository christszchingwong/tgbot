import TelegramBot from 'node-telegram-bot-api';

const token = process.env.TELEGRAM_TOKEN;

const bot = new TelegramBot(token);

export default async (updateObjects: any) => {
    for(const updateObject of updateObjects.result) {
        const targetChatId = updateObject.message.chat.id;
        // notice these calls are promises which are not awaitable
        await bot.sendMessage(targetChatId, 'Hello!');
    }
    
    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          message: 'updates received successfully!',
        },
        null,
        2
      ),
    };
}