# tg bot with aws lambda

## concepts

|name|description|
|----|-----------|
|`token`| both `id` and `authentication` token to telegram bot |

## Setup

1. setup telegram bot  
Add `@botfather` at telegram. Get the bot token.
1. setup `serverless` framework  
```npm install -g serverless```
1. prepare environment variable `TELEGRAM_TOKEN` in shell & `.env` file
1. prepare access key to aws  
[Prepare AWS access key to serverless framework](https://serverless.com/framework/docs/providers/aws/guide/credentials#using-aws-access-keys)
1. deploy to aws  
```serverless deploy -v```
1. setup webhook for the telegram bot to call AWS lambda  
