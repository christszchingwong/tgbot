import tgbot from '../src/tgbot';
const sampleUpdate = require('./resources/sampleUpdate.json');

test('process sample update', async() => {
    const result = await tgbot(sampleUpdate);
    expect(result).toBeDefined();
});