import hello from '../src/hello';

test('return simple object', async() => {
    const result = await hello(null);
    expect(result.statusCode).toBe(200);
});